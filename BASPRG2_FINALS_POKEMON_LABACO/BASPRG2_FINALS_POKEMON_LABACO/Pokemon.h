#ifndef POKEMON_H
#define POKEMON_H
#include <string>

using namespace std;

class Pokemon
{
public:
	Pokemon(string name_, int baseHp_, int level_, int expToNextLevel_, int baseDam_, string type_);

	void attack(Pokemon* wildPokemon);
	void takeDamage(Pokemon* wildPokemon);
	void gainExp();
	void leveUp();
	void listOfPokemon(int entry);

	string name;
	int baseHp;
	int hp;
	int level;
	int exp;
	int expToNextLevel;
	int baseDam;
	string type;

};
#endif