#include "Map.h"
#include <iostream>
#include <string>

Map::Map()
{
}

void Map::getLocation(Trainer* pokemonTrainer, string playerLocation)
{
	if (pokemonTrainer->yPos <= 2 && pokemonTrainer->yPos >= -2 && pokemonTrainer->xPos >= -2 && pokemonTrainer->xPos <= 2) {
		playerLocation = "Pallet Town";
		cout << "You are in " << playerLocation << endl;
		isSafe = true;
	}
	else if (pokemonTrainer->yPos <= 7 && pokemonTrainer->yPos >= 3 && pokemonTrainer->xPos >= -2 && pokemonTrainer->xPos <= 2) {
		playerLocation = "Route 1";
		cout << "You are in " << playerLocation << endl;
		isSafe = false;
	}
	else if (pokemonTrainer->yPos <= 12 && pokemonTrainer->yPos >= 8 && pokemonTrainer->xPos >= -2 && pokemonTrainer->xPos <= 2) {
		playerLocation = "Viridian City";
		cout << "You are in " << playerLocation << endl;
		isSafe = true;
	}
	else if (pokemonTrainer->yPos > 12 || pokemonTrainer->yPos < -2 || pokemonTrainer->xPos > 2 || pokemonTrainer->xPos < -2) {
		playerLocation = "Unknown Location";
		cout << "You are in " << playerLocation << endl;
	}
}


