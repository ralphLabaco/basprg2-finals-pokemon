#ifndef TRAINER_H
#define TRAINER_H
#include <string>

using namespace std;

class Trainer {

public:
	Trainer(string name_);

	void choosePokemon(int choice);
	void playerMove(char moveChoice);
	void displayPokemonStats();
	void goToPokemonCenter();


	string name;
	char movement;
	int choice;
	int xPos = 0;
	int yPos = 0;
};

#endif