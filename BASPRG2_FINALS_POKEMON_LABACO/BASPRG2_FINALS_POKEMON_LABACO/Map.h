#ifndef MAP_H
#define MAP_H
#include <string>
#include "Trainer.h"

using namespace std;

class Map
{

public:
	Map();

	void getLocation(Trainer* pokemonTrainer, string playerLocation);
	void pokemonEncounter();

	string location;
	bool isSafe;
};

#endif
