#include <iostream>
#include <string>
#include <time.h>
#include "Trainer.h"
#include "Map.h"

using namespace std;

void displayStartScreen() {
	cout << "___________           ___                                                 " << endl;
	cout << "|  ______  |          | |                                                 " << endl;
	cout << "|  |     | |          | |   _             _                     _         " << endl;
	cout << "|  |_____| |          | |  / /  _______  | |________           | |_______ " << endl;
	cout << "|  ________| ________ | |_/ /  / ____  | |          | _______  |   ___   |" << endl;
	cout << "|  |         |   _  | |    /   | |___|_| |  __  __  | |   _  | |  |   |  |" << endl;
	cout << "|  |         |  |_| | |  _  |  | |____   |  ||  ||  | |  |_| | |  |   |  |" << endl;
	cout << "|__|         |______| |_| |_|  |_____/   |__||__||__| |______| |__|   |__|" << endl;
	cout << endl;
	cout << " ________                             _               _                     " << endl;
	cout << "/   _____|                           | |             | |                    " << endl;
	cout << "| /         _   _                    | |           __| |__          _       " << endl;
	cout << "| |______  |_| | |________   _    _  | |  _______ |__   __|        | |____  " << endl;
	cout << "|______  |  _  |          | | |  | | | | /  __   |   | |  _______  |  __  | " << endl;
	cout << "__     | | | | |  __  __  | | |  | | | | | |  |  |   | |  |   _  | | /  |_| " << endl;
	cout << "| |____/ / | | |  ||  ||  | | |__| | | | | |__|| |   | |  |  |_| | | |      " << endl;
	cout << "|_______/  |_| |__||__||__| |______| |_| |_____|_|   |_|  |______| |_|      " << endl;
	cout << endl;
	cout << "A BASPRG2 SAMPLE PROJECT" << endl;
	system("pause");
	system("cls");
}
void selectFromMainMenu(Trainer* pokeTrainer) {
	cout << "What would you like to do?" << endl;
	cout << "[1] - Move" << endl << "[2] - Pokemon" << endl << "[3] - Pokemon Center" << endl;
	cin >> pokeTrainer->choice;
	system("cls");
	if (pokeTrainer->choice == 1) {
		Map* map = new Map();
		pokeTrainer->playerMove(pokeTrainer->movement);
		map->getLocation(pokeTrainer, map->location);
		delete map;
		map = NULL;
	}
	else if (pokeTrainer->choice == 2) {
		pokeTrainer->displayPokemonStats();
	}
	else if (pokeTrainer->choice == 3) {
	}
	else {
		cout << "You entered none of the options" << endl;
	}
}

int main() {
	displayStartScreen();

	string name;
	cout << "Hi Trainer! What's your name?" << endl;
	cin >> name;
	system("cls");

	Trainer* pokemonTrainer = new Trainer(name);
	pokemonTrainer->choosePokemon(pokemonTrainer->choice);

	cout << "Your Pokemon journey begins now" << endl;
	system("pause");
	system("cls");

	while (true) {
		selectFromMainMenu(pokemonTrainer);

		system("pause");
		system("cls");

	}

}